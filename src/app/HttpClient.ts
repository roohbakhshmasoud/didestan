/**
 * Created by Masoud on 11/25/2017.
 */



import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {LocalStorageService} from "angular-2-local-storage";

@Injectable()
export class HttpClient {

    private token = null;

    constructor(private http: Http,
                private localStorage: LocalStorageService ) {
    }

    createAuthorizationHeader(headers: Headers) {
        this.token = this.localStorage.get('token');
        headers.append('Authorization', 'Bearer ' + this.token);
    }

    get(url) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        });
    }

    post(url, data = null) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        });
    }

    put(url, data) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        });
    }
}