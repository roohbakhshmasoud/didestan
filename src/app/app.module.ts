import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';
import { SidebarModule } from 'ng-sidebar';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import {IndexModule} from './index/index.module';
import {IndexService} from './index/index.service';
import {AppConfig} from './app.config';
import {DashboardModule} from "./dashboard/dashboard.module";
import {CampaignModule} from "./campaign/campaign.module";
import {ManageCampaignModule} from "./manage-campaign/manage-campaign.module";
import {ManageCampaignService} from "./manage-campaign/manage-campaign.service";
import {ChannelModule} from "./channel/channel.module";
import {ChannelService} from "./channel/channel.service";
import {AdvertiseModule} from "./advertise/advertise.module";
import {AdvertiseService} from "./advertise/advertise.service";
import {PreloaderComponent} from "./preloader/preloader.component";
import {PreloaderModule} from "./preloader/preloader.module";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {CampaignService} from "./campaign/campaign.service";
import {LocalStorageModule} from "angular-2-local-storage";
import {RouteGaurd} from "./RouteGaurd";
import {ReportModule} from "./report/report.module";
import {ReportService} from "./report/report.service";
import {HttpClient} from "./HttpClient";
import {AdminModule} from "./admin/admin.module";
import {AdminService} from "./admin/admin.service";



@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    HttpModule,
    IndexModule,
    DashboardModule,
    CampaignModule,
    ChannelModule,
    ManageCampaignModule,
    AdvertiseModule,
    ReportModule,
    AdminModule,
    NgbModule.forRoot(),
    SidebarModule.forRoot(),
    BrowserAnimationsModule,
    PreloaderModule.forRoot(),
    LocalStorageModule.withConfig({
      prefix: 'app',
      storageType: 'localStorage'
    })
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},
                        IndexService, AppConfig, ManageCampaignService , ChannelService , AdvertiseService,
                        CampaignService, ReportService , RouteGaurd , HttpClient , AdminService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
