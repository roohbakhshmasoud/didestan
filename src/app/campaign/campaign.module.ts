import { NgModule ,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignComponent } from './campaign.component';
import {NgbDateStruct, NgbModule ,NgbCalendar, NgbDatepickerI18n, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {NgbCalendarPersian} from "ng2-datepicker-jalali/persian/ngb-calendar-persian";
import {NgbDatepickerI18nPersian} from "ng2-datepicker-jalali/persian/ngb-datepicker-i18n-persian";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {FileDropDirective, FileSelectDirective} from "ng2-file-upload";
import { FileUploadModule} from 'ng2-file-upload';
import {PreloaderModule} from "../preloader/preloader.module";
import {LazyLoadImageModule} from "ng-lazyload-image";

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    FormsModule,
    AngularMultiSelectModule,
    CampaignRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    LazyLoadImageModule,
    PreloaderModule
  ],
  declarations: [CampaignComponent],
  exports: [ReactiveFormsModule],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    {provide: NgbCalendar, useClass: NgbCalendarPersian},
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nPersian}
  ]
})
export class CampaignModule { }
