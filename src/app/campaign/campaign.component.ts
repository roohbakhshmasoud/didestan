import {Component, OnInit, ViewChild} from '@angular/core';
import {
    NgbDateStruct, NgbCalendar, NgbDatepickerI18n, NgbDatepickerConfig,
    NgbTabChangeEvent, NgbTabset , NgbModal, ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {NgbCalendarPersian} from "ng2-datepicker-jalali/persian/ngb-calendar-persian";
import {NgbDatepickerI18nPersian} from "ng2-datepicker-jalali/persian/ngb-datepicker-i18n-persian";
import {FileUploader, FileUploaderOptions} from "ng2-file-upload";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {AdvertiseService} from "../advertise/advertise.service";
import {AppConfig} from "../app.config";
import {ChannelService} from "../channel/channel.service";
import {CampaignService} from "./campaign.service";
import {isNumeric} from "rxjs/util/isNumeric";
import {LocalStorageService} from "angular-2-local-storage";



@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css'],
  providers: [
    {provide: NgbCalendar, useClass: NgbCalendarPersian},
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nPersian}
  ]
})
export class CampaignComponent implements OnInit {
    @ViewChild('t') ngbTabSet;

  campaignId: number;
  currentTab: number;
  confirmInfoTab  = true;

  searchKey = '' ;
  page = 1;
  total = null;
  perpage = 8;

  channelsList = [];
  displayChannelList = [];
  dropdownList = [];
  dropdownSettings = {};
  steps = { 2: false , 3: false , 4: false};
  informations = {id: null , advertisePerson : [] , selectedChannels : [], redirectUrl : null,
                  startTime: {hour: 10, minute: 0, second: 0 } , endTime: {hour: 11, minute: 0, second: 0 },
                  campaignName : null , budget : null , startDate: null , endDate : null , file: null};
  isEdit = false;


  preloaderVisible = false;
  message = 'در حال دریافت اطلاعات ...';
  preloaderImg = 'loader.gif';
  preloaderColor = 'black';

  // now = new Date();
  UploadFileUrl = this.baseConfig.uplaodApi ;
  isAdmin = null;
  apiKey = null;
  vastApi = null;






  private today: NgbDateStruct;
  public uploader: FileUploader = new FileUploader({url: this.UploadFileUrl, method: 'POST'});
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  private sub: any;



  constructor(calendar: NgbCalendar,
              config: NgbDatepickerConfig,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private adsService: AdvertiseService,
              private channelService: ChannelService,
              private campaignService: CampaignService,
              public baseConfig: AppConfig,
              public locaStorage: LocalStorageService ,
              ) {

    this.isAdmin = this.locaStorage.get('admin');

    this.today = calendar.getToday();
    this.currentTab = 1;
      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };



  }

  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: true,
      text:"انتخاب تبلیغ دهنده",
      selectAllText:'انتخاب همه',
      unSelectAllText:'غیرفعال کردن همه',
      enableSearchFilter: true,
      classes:"myclass"
    };
    this.getAdvertiserList();
    this.getChannels();

    this.sub = this.route.params.subscribe(params => {
      this.campaignId = +params['id'];
      if (!isNaN(this.campaignId)) {
          this.getAdvertise(this.campaignId);
      }

    });
  }

  onItemSelect(item:any){

  }
  OnItemDeSelect(item:any){

  }

  channelPageChange() {
    const start = this.page === 1 ? 0 : ((this.page * 8) - 8);
    const end = this.page * 8;
    this.displayChannelList = this.channelsList.slice(start, end);
  }
  search() {
  }
  channelChecked(channel) {
    if(channel.checked === true)
      this.informations.selectedChannels.push(channel);
    else{
      if(this.informations.selectedChannels.length === 0)
        return;
      for(let i=0; i < this.informations.selectedChannels.length; i++){
        if(this.informations.selectedChannels[i].title === channel.title)
          this.informations.selectedChannels.splice(i , 1);
      }
    }
  }

  ngOnDestroy() {
        this.sub.unsubscribe();
  }
  nextTab() {
      this.currentTab = this.currentTab + 1 ;
      this.ngbTabSet.select(String(this.currentTab));

      if(this.currentTab === 3 && this.informations.id === null){
          this.currentTab = 1;
          this.ngbTabSet.select(String(this.currentTab));
      }

      if (this.currentTab === 3 &&  this.informations.id === null) {
          this.createCampaign();
      }

    }
  previewsTab() {
        this.currentTab = this.currentTab - 1 ;
        this.ngbTabSet.select(String(this.currentTab));
    }
  getChannels() {
      this.displayChannelList = [];
      this.preloaderVisible = true;
      this.channelService.getAllChannels({'per_page': this.perpage , 'page': this.page , 'searchKey' : this.searchKey }).then((data) => {
          this.total = data.total;
          const channels = data.data;
          for (let index in channels){
              this.displayChannelList.push({"img" : this.baseConfig.baseApiRoot + channels[index][this.baseConfig.apiCreateChannelMap.img_uri.serverValue],
                                     "title" : channels[index][this.baseConfig.apiCreateChannelMap.title.serverValue],
                                     "checked" : false,
                                     "id" : channels[index][this.baseConfig.apiCreateChannelMap.identifier.serverValue],
                                     "price" : channels[index][this.baseConfig.apiCreateChannelMap.inCome.serverValue]});

          }
          this.preloaderVisible = false;
      }).catch((error) => {
         console.log(error);
          this.preloaderVisible = false;
      });

  }
  getAdvertiserList() {
      this.preloaderVisible = true;
      this.adsService.getAdvertise({'per_page': 10000000 , 'page': this.page , 'searchKey' : this.searchKey }).then((data) => {

          const advertisers = data.data;
          for(let item in advertisers){
              this.dropdownList.push({"id" : advertisers[item][this.baseConfig.apiAdvertiserList.id.serverValue],
                                      "itemName" : advertisers[item][this.baseConfig.apiAdvertiserList.title.serverValue]});

          }
          this.preloaderVisible = false;
      }).catch((error)=>{
         console.log(error) ;
          this.preloaderVisible = false;
      });
  }
  changeTab($event) {
      this.currentTab = Number($event.nextId);

      if (this.currentTab === 3 &&  this.informations.id === null) {
          this.createCampaign();
      }
  }
  getAdvertise(id) {
      // api call
      this.informations = {id: null , advertisePerson : [{"id":2,"itemName":"سامسونگ"},] , selectedChannels : [], redirectUrl : null ,
                           campaignName : 'test' , budget : '110' , startDate: null , endDate : null , file: null ,
                           startTime: {hour: 10, minute: 0, second: 0 } , endTime: {hour: 11, minute: 0, second: 0 } };
      this.isEdit = true;
  }
  confirmInfo() {
         // api call
        this.userSubmitCampaign();
    }
  updateInfo() {
    // api call
        this.router.navigateByUrl('dashboard/manageCampaign');
    }
  channelImageLoad(event) {
      // console.log(event);
  }

  createCampaign() {



      if ((this.informations.campaignName === null || this.informations.endDate === null ||
          this.informations.startDate === null || this.informations.redirectUrl === null ||
          this.informations.budget === null || this.informations.selectedChannels.length === 0) ||
          (this.isAdmin && this.informations.advertisePerson.length === 0 )) {
          this.preloaderVisible = true;
          this.message = 'اطلاعات کمپین را به صورت کامل وارد نمایید !!!';
          this.preloaderColor = 'red';
          this.preloaderImg = 'error.png';
          this.currentTab = 1;
          this.confirmInfoTab = true;
          setTimeout(() => {
              this.preloaderVisible = false;
          }, 1000);


            return;
          //should be prevent to tab change and active tab should be 1
      }
      if (!isNumeric(this.informations.budget)) {
          this.message = 'مبلغ بودجه را به درستی وارد نمایید .';
          this.preloaderVisible = true;
          this.preloaderImg = 'warning.png';
          this.preloaderColor = 'red';
          setTimeout(() => {
              this.preloaderVisible = false;
          } , 1000);
          return;
      }

      const channel_ids = [];
      for(let id in this.informations.selectedChannels) {
          channel_ids.push(this.informations.selectedChannels[id].id);
      }

      const startTime = this.informations.startTime.hour + ':' + this.informations.startTime.minute + ':' + this.informations.startTime.second;
      const endTime = this.informations.endTime.hour + ':' + this.informations.endTime.minute + ':' + this.informations.endTime.second;
      const startDate = this.informations.startDate.year +'-'+ this.informations.startDate.month +'-'+this.informations.startDate.day + ' ' + startTime;
      const endDate = this.informations.endDate.year +'-'+ this.informations.endDate.month +'-'+this.informations.endDate.day + ' ' + endTime ;
      const advertiserPerson = this.isAdmin ? this.informations.advertisePerson[0].id : null;

      const campaign = {[this.baseConfig.apiCampaignCreate.name.serverValue] : this.informations.campaignName,
                      [this.baseConfig.apiCampaignCreate.redirectUrl.serverValue] : this.informations.redirectUrl,
                      [this.baseConfig.apiCampaignCreate.startDate.serverValue] : startDate,
                      [this.baseConfig.apiCampaignCreate.endDate.serverValue] : endDate,
                      [this.baseConfig.apiCampaignCreate.selectedChannels.serverValue] : channel_ids,
                      [this.baseConfig.apiCampaignCreate.advertisePerson.serverValue] : advertiserPerson,
                      [this.baseConfig.apiCampaignCreate.budget.serverValue] : this.informations.budget} ;

      this.campaignService.createCampaign(campaign).then((data) => {
          this.informations.id = data[this.baseConfig.apiCampaignCreate.id.serverValue];
          this.UploadFileUrl = this.baseConfig.uplaodApi + this.informations.id;
          this.confirmInfoTab = false;
          this.getCampaignApiKey();
      }).catch((error) => {
          console.log(error);
      });
  }

  userSubmitCampaign() {
      this.message = 'کمپین شما با موفقیت در سیستم ثبت شد .';
      this.preloaderImg = 'success.png';
      this.campaignService.userActiveCampain(this.informations.id , { "status" : 1}).then((data) => {
          this.preloaderVisible = true ;
          setTimeout(() => {
              this.preloaderVisible = false;
              this.router.navigateByUrl('dashboard/manageCampaign') ;
          }, 2000);
      }).catch((error) => {
          console.log(error);
          this.preloaderVisible = false;
      });
  }
  uploadFile() {

      const token = this.locaStorage.get('token');
      const option: FileUploaderOptions = {} ;
      option.headers = [{ name: 'Authorization', value : 'Bearer ' + token  } ];
      this.uploader.setOptions(option);

      this.uploader.queue[0].url = this.UploadFileUrl;
      this.uploader.uploadAll();

  }
  getCampaignApiKey() {
      this.campaignService.getApiKey().then((data) => {
          this.apiKey = data.result;
          this.vastApi = this.baseConfig.baseApiRoot + this.baseConfig.vastApi + this.informations.id + '?api_key=' + this.apiKey;
          this.currentTab = this.currentTab + 1;
          this.ngbTabSet.select(String(this.currentTab ));
      }).catch((error) => {
         console.log(error);
      }) ;
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }



}
