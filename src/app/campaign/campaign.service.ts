import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Http} from "@angular/http";
import {HttpClient} from "../HttpClient";

@Injectable()
export class CampaignService {
  protected campaignCreateUrl = this.config.baseApiRoot + 'campaign';
  protected updateCampaignUrl = this.config.baseApiRoot + 'campaign';
  protected userActiveCampaignUrl = this.config.baseApiRoot + 'campaign/user/change-status/';
  protected adminActiveCampaignUrl = this.config.baseApiRoot + 'campaign/admin/change-status/';
  protected campaignApiKeyUrl = this.config.baseOuthRoot + 'api/createApiKey';

  protected listCampaignUrl = this.config.baseApiRoot + 'campaign';

  constructor(private config: AppConfig,
              private http: HttpClient ) { }

  createCampaign(campaign) {
  return this.http.post(this.campaignCreateUrl, campaign)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  updateCampain(campaign) {
    return this.http.put(this.updateCampaignUrl, campaign)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }

  userActiveCampain(id, status) {
    return this.http.put(this.userActiveCampaignUrl + id , {"status" :status})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }

  adminActiveCampain(id, status) {
    return this.http.put(this.adminActiveCampaignUrl + id , {"status" :status})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }

  getApiKey() {
    return this.http.post(this.campaignApiKeyUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
