import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {IndexService} from './index.service';
import {Router} from '@angular/router';
import {AppConfig} from '../app.config';
import {LocalStorageService} from 'angular-2-local-storage';


@Component ({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  currentJustify = 'justified';
  message = 'در حال ارسال اطلاعات ... ';
  preloaderVisible = false;
  preloaderImg = 'loader.gif';
  preloaderColor = 'black';
  isRegister = false;
  loginIsRegister = true;

  loginForm: FormGroup;
  registerForm: FormGroup;



  constructor(private fb: FormBuilder,
              public config: AppConfig,
              private srIndex: IndexService,
              private route: Router,
              private localStorage: LocalStorageService) {
    this.createLoginForm();
    this.createRegisterForm();
    this.isLogin();
  }

  ngOnInit() {
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['', Validators.required ],
      code: [''],
    });
  }
  createRegisterForm() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required ],
      email: ['', Validators.required ],
      password: ['', Validators.required ],
      repeatPassword: ['', Validators.required ],
      code: [''],
    });
  }
  login(user) {
    this.preloaderVisible = true;
    this.message = 'در حال ارسال اطلاعات ...';
    this.preloaderImg = 'loader.gif';

    this.preloaderColor = 'black';
    this.srIndex.login(user).then((data) => {

    if (data.hasOwnProperty(this.config.apiUserLoginMap.token.serverValue)) {
      this.localStorage.set('token', data[this.config.apiUserLoginMap.token.serverValue]);
      this.localStorage.set('admin', false);
      this.route.navigateByUrl('dashboard');
    }else{
      this.preloaderVisible = true;
      this.message = 'بروز خطا !!!!';
      this.preloaderColor = 'red';
      this.preloaderImg = 'warning.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
    }
    this.preloaderVisible = false;
    }).catch((err) => {
      console.log(err);
      if(err.status === 403 ) {
        this.loginIsRegister = false;
        this.preloaderVisible = true;
        this.message = 'لطفا کد فعال سازی را وارد نمایید !!!';
        this.preloaderColor = 'red';
        this.preloaderImg = 'error.png';
        setTimeout(() => {
          this.preloaderVisible = false;
        }, 2000);
        return;
      }
      this.preloaderVisible = true;
      this.message = 'نام کاربری یا رمز عبور اشتباه وارد شده است.';
      this.preloaderColor = 'red';
      this.preloaderImg = 'error.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
    });
  }
  register() {
    this.preloaderVisible = true;
    this.message = 'در حال ارسال اطلاعات ...';
    this.preloaderImg = 'loader.gif';
    this.preloaderColor = 'black';

    let user = this.registerForm.getRawValue();

    if (user.email === "" || user.password === "" || user.repeatPassword === "" || user.name === "") {
      this.message = 'فیلدها را به صورت درست وارد نمایید ...' ;
      this.preloaderVisible = true;
      this.preloaderImg = 'warning.png';
      this.preloaderColor = 'red';
      setTimeout(() => {
        this.preloaderVisible = false;
      } , 2000);
      return;
    }
    if (user.password !== user.repeatPassword) {
      this.preloaderVisible = true;
      this.message = 'کلمه های عبور وارد شده یکسان نمی باشند !';
      this.preloaderColor = 'red';
      this.preloaderImg = 'error.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
      return;
    }

    if (user.password.length <= 5) {
      this.preloaderVisible = true;
      this.message = 'کلمه عبور نباید کمتر از 6 کاراکتر باشد !';
      this.preloaderColor = 'red';
      this.preloaderImg = 'error.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
      return;
    }

    user = { [this.config.apiUserRegister.name.serverValue] : user.name,
              [this.config.apiUserRegister.email.serverValue] : user.email ,
              [this.config.apiUserRegister.password.serverValue] : user.password} ;

    this.srIndex.register(user).then((data) => {
      if(data.hasOwnProperty(this.config.apiUserRegister.verify.serverValue)
          && data[this.config.apiUserRegister.verify.serverValue] === false) {
        this.isRegister = true;
      }else if (data.hasOwnProperty(this.config.apiUserRegister.verify.serverValue)
          && data[this.config.apiUserRegister.verify.serverValue] === true) {
        this.isRegister = false;
        this.preloaderVisible = true;
        this.message = 'شما قبلا ثبت نام کرده اید ، لطفا به سیستم وارد شوید .';
        this.preloaderColor = 'red';
        this.preloaderImg = 'warning.png';
        setTimeout(() => {
          this.preloaderVisible = false;
        }, 2000);
        return;
      }else {
        this.isRegister = false;
      }
      this.preloaderVisible = false;
    }).catch((err) => {
      console.log(err);
      this.isRegister = false;
      this.preloaderVisible = true;
      this.message = 'ایمیل وارد شده قبلا ثبت شده است !!!';
      this.preloaderColor = 'red';
      this.preloaderImg = 'error.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
    });
  }

  verification(user) {
    if (user.email === "" || user.password === "" || user.code === "") {
      this.message = 'فیلدها را به صورت درست وارد نمایید ...' ;
      this.preloaderVisible = true;
      this.preloaderImg = 'warning.png';
      this.preloaderColor = 'red';
      setTimeout(() => {
        this.preloaderVisible = false;
      } , 800);
      return;
    }
    let verification = { [this.config.apiUserVerify.email.serverValue] : user.email,
                         [this.config.apiUserVerify.code.serverValue] : user.code} ;
    this.srIndex.verificationUser(verification).then((data) => {
      this.login(user);
    }).catch((err) => {
      console.log(err);
    });
  }
  confirm() {
    if(this.isRegister) {
      let user = this.registerForm.getRawValue();
      this.verification(user);
    }
    else {
        this.register();
    }
  }

  confirmLogin() {
    if(!this.loginIsRegister) {
      let user = this.loginForm.getRawValue();
      this.verification(user);
    }
    else {
      const user = this.loginForm.getRawValue();
      this.login(user);
    }
  }
  isLogin() {
    const token = this.localStorage.get('token');
    if (token !== null) {
      this.route.navigateByUrl('dashboard');
    }
  }
}
