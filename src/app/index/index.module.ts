import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';


import { IndexRoutingModule } from './index-routing.module';
import { IndexComponent } from './index.component';
import {PreloaderModule} from "../preloader/preloader.module";


@NgModule({
  imports: [
    CommonModule,
    IndexRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PreloaderModule

  ],
  exports: [ReactiveFormsModule],
  declarations: [IndexComponent]
})
export class IndexModule { }
