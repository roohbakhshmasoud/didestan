import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {AppConfig} from '../app.config';


@Injectable()
export class IndexService {
  loginUrl = this.config.Oauth2.login_url;
  registerUrl = this.config.baseApiRoot + 'user/register';
  verificationUrl = this.config.baseApiRoot + 'user/verify';


  constructor(private http: Http,
              private config: AppConfig) { }

  login(user) {
    const options = { [this.config.apiUserLoginMap.clientId.serverValue] : this.config.Oauth2.client_id ,
                     [this.config.apiUserLoginMap.grantType.serverValue] : this.config.Oauth2.grant_type ,
                     [this.config.apiUserLoginMap.username.serverValue] : user.email ,
                     [this.config.apiUserLoginMap.password.serverValue] : user.password };

  return this.http.post(this.loginUrl , options )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }
  register(user) {
    return this.http.post(this.registerUrl, user)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  verificationUser(user) {
    return this.http.post(this.verificationUrl, user)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
