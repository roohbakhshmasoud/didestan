import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChannelRoutingModule } from './channel-routing.module';
import { ChannelComponent } from './channel.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import { ChannelViewComponent } from './channel-view/channel-view.component';
import {FileUploadModule} from "ng2-file-upload";
import {LazyLoadImageModule} from "ng-lazyload-image";
import {PreloaderModule} from "../preloader/preloader.module";


@NgModule({
  imports: [
    CommonModule,
    ChannelRoutingModule,
    NgbModule,
    FormsModule,
    FileUploadModule,
    LazyLoadImageModule,
    PreloaderModule,

  ],
  declarations: [ChannelComponent, ChannelViewComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class ChannelModule { }
