import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Headers, Http, RequestOptions} from "@angular/http";
import {HttpClient} from "../HttpClient";

@Injectable()
export class ChannelService {
  private channelListUrl = this.config.baseApiRoot + 'channel';
  private channelAllUrl = this.config.baseApiRoot + 'channel/all';
  private createChannelUrl = this.config.baseApiRoot + 'channel';
  private updateChannelUrl = this.config.baseApiRoot + 'updateChannel';
  private singleChannelUrl = this.config.baseApiRoot + 'channel';
  constructor(private config: AppConfig,
              private http: HttpClient ) {}
  getChannels(options) {

    return this.http.get(this.channelListUrl + '?page=' + options.page + '&per_page=' + options.per_page + '&q=' + options.searchKey )
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  getAllChannels(options) {
    return this.http.get(this.channelAllUrl + '?page=' + options.page + '&per_page=' + options.per_page + '&q=' + options.searchKey )
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  getSingleChannel(id) {
    return this.http.get(this.singleChannelUrl + '/' + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  createChannel(channel) {
    const headers = new Headers();
    const formData = new FormData();
    // formData.append('img', channel.img, 'test.jpg');
    // formData.append('upstream_identifier', channel.upstream_identifier);
    // formData.append('name', channel.name);
    for ( const key in channel ) {
      formData.append(key, channel[key]);
    }
    return this.http.post(this.createChannelUrl, formData )
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }
  updateChannel(channel) {
    return this.http.put(this.updateChannelUrl, channel)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
