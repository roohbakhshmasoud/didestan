import { Component, OnInit } from '@angular/core';
import {ChannelService} from "../channel.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AppConfig} from "../../app.config";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-channel-view',
  templateUrl: './channel-view.component.html',
  styleUrls: ['./channel-view.component.css']
})
export class ChannelViewComponent implements OnInit {

  private sub;
  private channelId;
  public channel: {};

  displayCampaigns= [] ;
  page= 1;
  campaignLength:number;
  dataIsLoad = false;
  preloaderVisible = false;
  preloaderMessage = 'در حال دریافت اطلاعات ...';
  preloaderImg = 'preloader.gif';

  constructor(private service: ChannelService,
              private route: ActivatedRoute,
              public config: AppConfig) {

      }

  ngOnInit() {
    this.getIdFromUrl();
  }
  getIdFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.channelId = +params['id'];
      if (!isNaN(this.channelId)) {
        this.getChannelInfo(this.channelId);
      }});
  }
  getChannelInfo(id) {
    this.preloaderVisible = true;
    this.preloaderImg = 'preloader.gif';
    const campaignsList = [];
    this.service.getSingleChannel(id).then((data) => {
      const channel = {'id' : data[this.config.apiCreateChannelMap.identifier.serverValue] ,
                       'title' : data[this.config.apiCreateChannelMap.title.serverValue]  ,
                       'price' : data[this.config.apiCreateChannelMap.price.serverValue]  ,
                       'inCome' : data[this.config.apiCreateChannelMap.inCome.serverValue] ,
                       'channelId' : data[this.config.apiCreateChannelMap.channelId.serverValue] ,
                       'campaigns' : data[this.config.apiCreateChannelMap.campaigns.serverValue].length,
                       'img': data[this.config.apiCreateChannelMap.img_uri.serverValue],
                       'activeCampaigns': 0 } ;

      const campaigns = data[this.config.apiCreateChannelMap.campaigns.serverValue];

      for (let campaign in campaigns) {
          const startDate = campaigns[campaign][this.config.apiCampaignList.startDate.serverValue].split(' ');
          const endDate = campaigns[campaign][this.config.apiCampaignList.endDate.serverValue].split(' ');

          let item = {[this.config.apiCampaignList.id.frontValue] : campaigns[campaign][this.config.apiCampaignList.id.serverValue],
              [this.config.apiCampaignList.endDate.frontValue] : endDate[0],
              [this.config.apiCampaignList.startDate.frontValue] : startDate[0],
              [this.config.apiCampaignList.budget.frontValue] : campaigns[campaign][this.config.apiCampaignList.budget.serverValue],
              [this.config.apiCampaignList.activeAdmin.frontValue] : campaigns[campaign][this.config.apiCampaignList.activeAdmin.serverValue],
              [this.config.apiCampaignList.activeUser.frontValue] : campaigns[campaign][this.config.apiCampaignList.activeUser.serverValue],
              [this.config.apiCampaignList.remainedBudget.frontValue] : campaigns[campaign][this.config.apiCampaignList.remainedBudget.serverValue],
              [this.config.apiCampaignList.viewCount.frontValue] : campaigns[campaign][this.config.apiCampaignList.viewCount.serverValue],
              [this.config.apiCampaignList.channels.frontValue] : campaigns[campaign][this.config.apiCampaignList.channels.serverValue],
              [this.config.apiCampaignList.name.frontValue] : campaigns[campaign][this.config.apiCampaignList.name.serverValue]};

          if (campaigns[campaign][this.config.apiCampaignList.activeAdmin.serverValue] === 1)
              channel.activeCampaigns =  channel.activeCampaigns +1;
        campaignsList.push(item);
      }



      this.channel = channel;
      this.channel['campaignsList'] = campaignsList;
      this.displayCampaigns = this.channel['campaignsList'].slice(0, 8);
      this.campaignLength = this.channel['campaignsList'].length;
      this.dataIsLoad = true;
      this.preloaderVisible = false;
    }).catch((err) => {
      this.preloaderImg = 'error.png';
      this.preloaderMessage = 'رکورد مورد نظر یافت نشد !!!' ;
      this.dataIsLoad = false;
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000)

    });
  }
  channelPageChange() {
    const start = this.page === 1 ? 0 : ((this.page * 8) - 8);
    const end = this.page * 8;
    this.displayCampaigns = this.channel['campaignsList'].slice(start, end);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
