import { Component, OnInit } from '@angular/core';
import {ChannelService} from "./channel.service";
import {FileUploader} from "ng2-file-upload";
import {AppConfig} from "../app.config";
import {PreloaderComponent} from "../preloader/preloader.component";
import {LocalStorageService} from "angular-2-local-storage";

const URL = 'httpszz://evening-anchorage-3159.herokuapp.com/api/';


@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css'],
})
export class ChannelComponent implements OnInit {

    //uploader section
    public uploader: FileUploader = new FileUploader({url: URL});
    public hasBaseDropZoneOver:boolean = false;
    public hasAnotherDropZoneOver:boolean = false;
    private sub: any;

    // end
    public mask ;
  channels = [];
  displayChannels = [];
  channel = { id: null , channelId: null , title: null , price: null , inCome: null , campaigns: null , activeCampaign: null };
  isCreate = false;
  page = 1;
  total = null;
  perpage = 10;
  searchKey = '';
  file = null;

  preloaderVisible = false;
  message = 'در حال دریافت اطلاعات ...';
  preloaderImg = 'loader.gif';
  preloaderColor = 'black';
  filename = 'تصویر کانال' ;
  isAdmin = null;



  constructor(private service: ChannelService,
              public config: AppConfig,
              public localsotrage: LocalStorageService
              ) {
    this.isAdmin = this.localsotrage.get('admin');
  }
  ngOnInit() {
    if(this.isAdmin) {
        this.getAllChannels();
    }else{
        this.getChannels();
    }
  }
  getChannels() {
    this.displayChannels = [];
    this.preloaderVisible = true;
    this.service.getChannels({'per_page': this.perpage , 'page': this.page , 'searchKey' : this.searchKey }).then((data) => {
        this.total = data[this.config.apiCreateChannelMap.total.serverValue];
        const channels = data.data;
        if (data.length === 0) {
            this.preloaderVisible = false;
            return;
        }
        let index = 0;
        this.channels = [];
        for (const channel in channels) {
            this.channels.push({[this.config.apiCreateChannelMap.file.frontValue] : channels[index][this.config.apiCreateChannelMap.file.serverValue],
                [this.config.apiCreateChannelMap.channelId.frontValue] : channels[index][this.config.apiCreateChannelMap.channelId.serverValue],
                [this.config.apiCreateChannelMap.title.frontValue] : channels[index][this.config.apiCreateChannelMap.title.serverValue],
                [this.config.apiCreateChannelMap.inCome.frontValue] : channels[index][this.config.apiCreateChannelMap.inCome.serverValue],
                [this.config.apiCreateChannelMap.price.frontValue] : channels[index][this.config.apiCreateChannelMap.price.serverValue],
                [this.config.apiCreateChannelMap.id.frontValue] : channels[index][this.config.apiCreateChannelMap.identifier.serverValue],
                [this.config.apiCreateChannelMap.viewCount.frontValue] : channels[index][this.config.apiCreateChannelMap.viewCount.serverValue],
                [this.config.apiCreateChannelMap.img_uri.frontValue] : channels[index][this.config.apiCreateChannelMap.img_uri.serverValue]});
            index = index + 1 ;

        }
        this.displayChannels = this.channels;
        this.preloaderVisible = false;
    }).catch((error) => {
      console.log(error);
        this.preloaderVisible = false;

    });
  }

  getAllChannels() {
        this.preloaderVisible = true;
        const options = {page : 1 , per_page : 1000000 , searchKey : ''};
        this.service.getAllChannels(options).then((data) => {
            this.total = data[this.config.apiCreateChannelMap.total.serverValue];
            const channels = data.data;
            if (data.length === 0) {
                this.preloaderVisible = false;
                return;
            }
            let index = 0;
            this.channels = [];
            for (const channel in channels) {
                this.channels.push({[this.config.apiCreateChannelMap.file.frontValue] : channels[index][this.config.apiCreateChannelMap.file.serverValue],
                    [this.config.apiCreateChannelMap.channelId.frontValue] : channels[index][this.config.apiCreateChannelMap.channelId.serverValue],
                    [this.config.apiCreateChannelMap.title.frontValue] : channels[index][this.config.apiCreateChannelMap.title.serverValue],
                    [this.config.apiCreateChannelMap.inCome.frontValue] : channels[index][this.config.apiCreateChannelMap.inCome.serverValue],
                    [this.config.apiCreateChannelMap.price.frontValue] : channels[index][this.config.apiCreateChannelMap.price.serverValue],
                    [this.config.apiCreateChannelMap.id.frontValue] : channels[index][this.config.apiCreateChannelMap.identifier.serverValue],
                    [this.config.apiCreateChannelMap.viewCount.frontValue] : channels[index][this.config.apiCreateChannelMap.viewCount.serverValue],
                    [this.config.apiCreateChannelMap.img_uri.frontValue] : channels[index][this.config.apiCreateChannelMap.img_uri.serverValue]});
                index = index + 1 ;

            }
            this.displayChannels = this.channels;
            this.preloaderVisible = false;
        }).catch((error) => {
            console.log(error);
            this.preloaderVisible = false;
        });

    }

  createChannel() {
    const channel = {[this.config.apiCreateChannelMap.file.serverValue] : this.file,
                     [this.config.apiCreateChannelMap.channelId.serverValue] : this.channel.channelId ,
                     [this.config.apiCreateChannelMap.title.serverValue] : this.channel.title ,
                     [this.config.apiCreateChannelMap.inCome.serverValue] : this.channel.inCome ,
                     [this.config.apiCreateChannelMap.price.serverValue] : this.channel.price } ;

    if (this.file === null || this.channel.inCome === null || this.channel.price  === null ||
        isNaN(this.channel.price)  || isNaN(this.channel.inCome) ) {
        this.message = 'فیلدها را به صورت درست وارد نمایید ...' ;
        this.preloaderVisible = true;
        this.preloaderImg = 'warning.png';
        this.preloaderColor = 'red';
        setTimeout(() => {
            this.preloaderVisible = false;
        } , 800);
        return;
    }
    if (this.channel.inCome >= this.channel.price) {
        this.message = 'درآمد هر بازدید ، باید از قیمت هر بازدید کمتر باشد.';
        this.preloaderVisible = true;
        this.preloaderImg = 'warning.png';
        this.preloaderColor = 'red';
        setTimeout(() => {
            this.preloaderVisible = false;
        } , 800);
        return;
    }


    this.isCreate = false;
    this.message = 'در حال ارسال اطلاعات ...';
    this.preloaderVisible = true;
    this.service.createChannel(channel).then((data) => {
        this.getChannels();
      this.channels.push(data);
        this.preloaderVisible = false;
    }).catch((err) => {
      console.log(err);
        this.preloaderVisible = false;
    });
  }
  updateChannel(channel) {
    channel.isEdit = false;
    // this.service.updateChannel(this.channel).then((data) => {
    //   console.log(data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  }
  fileChange(event) {
      this.file = event.target.files[0];
      this.filename = this.file.name;
      console.log(this.file);
  }

}
