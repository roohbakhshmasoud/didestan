import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.css']
})
export class PreloaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() message: string;
  @Input() visible: boolean;
  @Input() img: string;
  @Input() color: string;
}
