import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Http} from "@angular/http";

@Injectable()
export class AdminService {
  adminLoginUrl = this.config.baseApiRoot + 'user/admin/token';
  constructor( private config: AppConfig ,
               private http: Http) { }


  login(user) {
    const options = { [this.config.apiUserLoginMap.username.serverValue] : user.email ,
                      [this.config.apiUserLoginMap.password.serverValue] : user.password };

    return this.http.post(this.adminLoginUrl , options )
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
