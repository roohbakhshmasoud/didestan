import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import {PreloaderModule} from "../preloader/preloader.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {IndexRoutingModule} from "../index/index-routing.module";

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    CommonModule,
    IndexRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PreloaderModule
  ],
  declarations: [AdminComponent]
})
export class AdminModule { }
