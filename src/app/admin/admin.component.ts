import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {Router} from '@angular/router';
import {AppConfig} from '../app.config';
import {LocalStorageService} from 'angular-2-local-storage';
import {AdminService} from "./admin.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  currentJustify = 'justified';
  message = 'در حال ارسال اطلاعات ... ';
  preloaderVisible = false;
  preloaderImg = 'loader.gif';
  preloaderColor = 'black';
  isRegister = false;
  loginIsRegister = true;

  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              public config: AppConfig,
              private srIndex: AdminService,
              private route: Router,
              private localStorage: LocalStorageService) {
    this.createLoginForm();
  }

  ngOnInit() {
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      code: [''],
    });
  }

  login() {
    const user = this.loginForm.getRawValue();

    this.preloaderVisible = true;
    this.message = 'در حال ارسال اطلاعات ...';
    this.preloaderImg = 'loader.gif';

    this.preloaderColor = 'black';

    this.srIndex.login(user).then((data) => {
      if (data.hasOwnProperty(this.config.apiUserLoginMap.token.serverValue)) {
        this.localStorage.set('token', data[this.config.apiUserLoginMap.token.serverValue]);
        this.localStorage.set('admin', true);
        this.route.navigateByUrl('dashboard');
      } else {
        this.preloaderVisible = true;
        this.message = 'بروز خطا !!!!';
        this.preloaderColor = 'red';
        this.preloaderImg = 'warning.png';
        setTimeout(() => {
          this.preloaderVisible = false;
        }, 2000);
      }
      this.preloaderVisible = false;
    }).catch((err) => {
      console.log(err);
      if (err.status === 403) {
        this.loginIsRegister = false;
        this.preloaderVisible = true;
        this.message = 'لطفا کد فعال سازی را وارد نمایید !!!';
        this.preloaderColor = 'red';
        this.preloaderImg = 'error.png';
        setTimeout(() => {
          this.preloaderVisible = false;
        }, 2000);
        return;
      }
      this.preloaderVisible = true;
      this.message = 'نام کاربری یا رمز عبور اشتباه وارد شده است.';
      this.preloaderColor = 'red';
      this.preloaderImg = 'error.png';
      setTimeout(() => {
        this.preloaderVisible = false;
      }, 2000);
    });
  }
}
