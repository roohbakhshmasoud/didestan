import { Component, OnInit } from '@angular/core';
import {ManageCampaignService} from "./manage-campaign.service";
import { Router} from "@angular/router";
import {AppConfig} from "../app.config";
import {split} from "ts-node/dist";
import {CampaignService} from "../campaign/campaign.service";
import {LocalStorageService} from "angular-2-local-storage";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-manage-campaign',
  templateUrl: './manage-campaign.component.html',
  styleUrls: ['./manage-campaign.component.css']
})
export class ManageCampaignComponent implements OnInit {

  campaignList = [];
  channelList = [];
  displayCampaignList = [];
  switchSettings = {};
  dropdownList = [];
  advertisePerson = [];
  dropdownSettings = {};

  page = 1;
  total = null;
  perpage = 10;
  searchKey = '';
  private activeAdmin = null;

 preloaderVisible = false;
 message = 'در حال دریافت اطلاعات ...';
 preloaderImg = 'loader.gif';
 preloaderColor = 'black';
 isAdmin = null;

 closeResult: string;
 vastApi = null;
 apiKey = null;

  constructor(private service: ManageCampaignService,
              private campaignService: CampaignService,
              private router: Router,
              private config: AppConfig,
              private localStorage: LocalStorageService,
              private modalService: NgbModal) {
    this.switchSettings = {
      onText: 'فعال' ,
      offText: 'غیرفعال'  ,
      onColor : 'blue'  ,
      offColor : 'red'  ,
      size : 'mini'  ,
    };
    this.isAdmin = this.localStorage.get('admin');
      this.getCampaignApiKey();
  }

  ngOnInit() {
    this.getCampaignList();
  }



  getCampaignList() {
    this.preloaderVisible = true;
    this.displayCampaignList = [];
    this.service.campaignList({'per_page': this.perpage , 'page': this.page , 'searchKey' : this.searchKey }).then(data => {
        this.total = data.total;
        const campaigns = data.data;
        for (const campaign in campaigns) {

            const startDate = campaigns[campaign][this.config.apiCampaignList.startDate.serverValue].split(' ');
            const endDate = campaigns[campaign][this.config.apiCampaignList.endDate.serverValue].split(' ');
            if (campaigns[campaign][this.config.apiCampaignList.activeAdmin.serverValue] === -1) {
                this.activeAdmin = 'reject';
            }
            if (campaigns[campaign][this.config.apiCampaignList.activeAdmin.serverValue] === 1) {
                this.activeAdmin = 'allow';
            }
            if (campaigns[campaign][this.config.apiCampaignList.activeAdmin.serverValue] === 0) {
                this.activeAdmin = 'watting';
            }

            this.displayCampaignList.push({[this.config.apiCampaignList.id.frontValue] : campaigns[campaign][this.config.apiCampaignList.id.serverValue],
                                            [this.config.apiCampaignList.endDate.frontValue] : endDate[0],
                                            [this.config.apiCampaignList.startDate.frontValue] : startDate[0],
                                            [this.config.apiCampaignList.budget.frontValue] : campaigns[campaign][this.config.apiCampaignList.budget.serverValue],
                                            [this.config.apiCampaignList.activeAdmin.frontValue] : this.activeAdmin,
                                            [this.config.apiCampaignList.activeUser.frontValue] : campaigns[campaign][this.config.apiCampaignList.activeUser.serverValue],
                                            [this.config.apiCampaignList.remainedBudget.frontValue] : campaigns[campaign][this.config.apiCampaignList.remainedBudget.serverValue],
                                            [this.config.apiCampaignList.viewCount.frontValue] : campaigns[campaign][this.config.apiCampaignList.viewCount.serverValue],
                                            [this.config.apiCampaignList.advertiser.frontValue] : campaigns[campaign][this.config.apiCampaignList.advertiser.serverValue][this.config.apiAdvertiserList.title.serverValue],
                                            [this.config.apiCampaignList.channels.frontValue] : campaigns[campaign][this.config.apiCampaignList.channels.serverValue],
                                            [this.config.apiCampaignList.name.frontValue] : campaigns[campaign][this.config.apiCampaignList.name.serverValue]});
        }
        this.preloaderVisible = false;
    }).catch((err) => {
        console.log(err);
        this.preloaderVisible = false;
    });
  }

  udpateCampaign(info) {
    // info.edit = true;
    console.log(info);
    this.router.navigateByUrl('dashboard/manageCampaign');
  }

  userChangeStatus($event, campaign) {
      campaign.activeUser = $event;
      if ($event === false)
          campaign.activeAdmin = 'watting';
      this.campaignService.userActiveCampain(campaign.id, $event).then((data) => {
      }).catch((error) => {
          console.log(error);
      });
  }
    adminChangeStatus(campaign) {
      let status = 0;
      if (campaign.activeAdmin === 'watting')
          status = 0;
      if (campaign.activeAdmin === 'allow')
          status = 1;
        if (campaign.activeAdmin === 'reject')
            status = -1;

        this.campaignService.adminActiveCampain(campaign.id , status).then((data) => {
        }).catch((error) => {
            console.log(error);
        });
    }

    open(campaignId) {
        this.vastApi = this.config.baseApiRoot + this.config.vastApi + campaignId + '?api_key=' + this.apiKey;
        this.modalService.open(this.vastApi).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    getCampaignApiKey() {
        this.campaignService.getApiKey().then((data) => {
            this.apiKey = data.result;
        }).catch((error) => {
            console.log(error);
        }) ;
    }

}
