import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Http} from "@angular/http";
import {HttpClient} from "../HttpClient";

@Injectable()
export class ManageCampaignService {
  campaignListUrl = this.config.baseApiRoot + 'campaign';
  updateCampaignUrl = this.config.baseApiRoot + 'campaign';

  constructor(private config: AppConfig,
              private http: HttpClient ) { }


  campaignList(options) {
    return this.http.get(this.campaignListUrl + '?page=' + options.page + '&per_page=' + options.per_page + '&q=' + options.searchKey)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  updateCampaign(data) {
    return this.http.put(this.updateCampaignUrl, data)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
