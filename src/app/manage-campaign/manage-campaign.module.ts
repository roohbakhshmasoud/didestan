import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageCampaignRoutingModule } from './manage-campaign-routing.module';
import { ManageCampaignComponent } from './manage-campaign.component';
import {BootstrapSwitchModule} from "angular2-bootstrap-switch";
import {AngularMultiSelectModule} from "angular2-multiselect-dropdown";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {PreloaderModule} from "../preloader/preloader.module";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    ManageCampaignRoutingModule,
    BootstrapSwitchModule,
    NgbModule,
    FormsModule,
    PreloaderModule
  ],
  declarations: [ManageCampaignComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports: [AngularMultiSelectModule]
})
export class ManageCampaignModule { }
