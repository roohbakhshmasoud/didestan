import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Http} from "@angular/http";
import {HttpClient} from "../HttpClient";

@Injectable()
export class AdvertiseService {
  private advertiseListUrl = this.config.baseApiRoot + 'user';
  private createAdvertiseUrl = this.config.baseApiRoot + 'createAdvertise';
  private updateAdvertiseUrl = this.config.baseApiRoot + 'updateAdvertise';
  private singleAdvertiseUrl = this.config.baseApiRoot + 'getAdvertise';

  constructor(private config: AppConfig,
              private http: HttpClient ) { }

  getAdvertise(options) {
    return this.http.get(this.advertiseListUrl + '?page=' + options.page + '&per_page=' + options.per_page + '&q=' + options.searchKey)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }
  getSingleAdvertise(id) {
    return this.http.get(this.singleAdvertiseUrl + '?id=' + id)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }
  createAdvertise(advertise) {
    return this.http.post(this.createAdvertiseUrl, advertise)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }
  updateAdvertise(advertise) {
    return this.http.put(this.updateAdvertiseUrl, advertise)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
