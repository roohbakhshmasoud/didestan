import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvertiseRoutingModule } from './advertise-routing.module';
import { AdvertiseComponent } from './advertise.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {PreloaderModule} from "../preloader/preloader.module";

@NgModule({
  imports: [
    CommonModule,
    AdvertiseRoutingModule,
    NgbModule,
    FormsModule,
    PreloaderModule
  ],
  declarations: [AdvertiseComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class AdvertiseModule { }
