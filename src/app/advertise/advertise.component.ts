import { Component, OnInit } from '@angular/core';
import {AdvertiseService} from "./advertise.service";
import {AppConfig} from "../app.config";

@Component({
  selector: 'app-advertise',
  templateUrl: './advertise.component.html',
  styleUrls: ['./advertise.component.css']
})
export class AdvertiseComponent implements OnInit {

  advertises = [];
  displayadvertises = [];
  advertise = { id: null , title: null , activeCampaign: null , campaignList: []};
  isCreate = false;
  page = 1;
  total = null;
  perpage = 10;
  searchKey = '';
  preloaderVisible = false;
  preloaderMessage = 'در حال دریافت اطلاعات ...';
  preloaderImg = 'loader.gif';

  constructor(private service: AdvertiseService,
              private config: AppConfig) { }
  ngOnInit() {
    this.getadvertises();
  }
  getadvertises() {
    this.preloaderVisible = true;
    this.service.getAdvertise({'per_page': this.perpage , 'page': this.page , 'searchKey' : this.searchKey }).then((data) => {
        const advertisers = data.data;
        this.total = data.total;
        if (data.length < 0) {
            return;
        }
        let index = 0;
        for (const channel in advertisers) {
            this.advertises.push({
                [this.config.apiAdvertiserList.id.frontValue]: advertisers[index][this.config.apiAdvertiserList.id.serverValue],
                [this.config.apiAdvertiserList.activeCampaign.frontValue]: advertisers[index][this.config.apiAdvertiserList.activeCampaign.serverValue],
                [this.config.apiAdvertiserList.title.frontValue]: advertisers[index][this.config.apiAdvertiserList.title.serverValue],
                [this.config.apiAdvertiserList.slug.frontValue]: advertisers[index][this.config.apiAdvertiserList.slug.serverValue],
                [this.config.apiAdvertiserList.campaignList.frontValue]: advertisers[index][this.config.apiAdvertiserList.campaignList.serverValue],
            });
            index = index + 1;
        }
        this.displayadvertises = this.advertises;
        this.preloaderVisible = false;
      // this.advertises = data;
    }).catch((error) => {
      console.log(error);
        this.preloaderVisible = false;
    });
  }
  createAdvertise() {
    this.advertises.push(this.advertise);
    this.isCreate = false;
    // this.service.createadvertise(this.advertise).then((data) => {
    //   this.advertises.push(data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  }
  updateAdvertise(advertise) {
    advertise.isEdit = false;
    // this.service.updateadvertise(this.advertise).then((data) => {
    //   console.log(data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  }
  advertisePageChange() {
    const start = this.page === 1 ? 0 : ((this.page * 8) - 8);
    const end = this.page * 8;
    this.displayadvertises = this.advertises.slice(start, end);
  }

}
