import { Component, OnInit } from '@angular/core';
import {ChannelService} from "../channel/channel.service";
import {AppConfig} from "../app.config";
import {ReportService} from "./report.service";
import {LocalStorageService} from "angular-2-local-storage";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  channels = [];
  isAdmin = null;

  constructor(private channelService: ChannelService,
              private reportService: ReportService,
              private config: AppConfig,
              private localstorage: LocalStorageService) {
    this.isAdmin = this.localstorage.get('admin');
    console.log(this.isAdmin);
    if (this.isAdmin)
      this.getAllChannels();
    else {
      this.getChannels();
    }
  }

  ngOnInit() {
  }

  // Pie
  public pieChartLabels:string[] = ['کل کمپین ها', 'کمپین های فعال', 'تعداد مشاهده'];
  public pieChartData:number[] = [0, 0, 0];
  public pieChartType:string = 'pie';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

  getChannels() {
    const options = {page : 1 , per_page : 1000000 , searchKey : ''};
    this.channelService.getChannels(options).then((data) => {
      const channels = data.data;
      for (const index in channels) {
        this.channels.push({"img" : this.config.baseApiRoot + channels[index][this.config.apiCreateChannelMap.img_uri.serverValue],
          "title" : channels[index][this.config.apiCreateChannelMap.title.serverValue],
          "checked" : false,
          "identifier" : channels[index][this.config.apiCreateChannelMap.identifier.serverValue],
          "price" : channels[index][this.config.apiCreateChannelMap.inCome.serverValue]});

      }
      // console.log(channels);
      this.getChannelReport(channels[0].identifier);
    }).catch ((error) => {
      console.log(error);
    });
  }

  getAllChannels() {
    const options = {page : 1 , per_page : 1000000 , searchKey : ''};
    this.channelService.getAllChannels(options).then((data) => {
      const channels = data.data;
      for (const index in channels) {
        this.channels.push({"img" : this.config.baseApiRoot + channels[index][this.config.apiCreateChannelMap.img_uri.serverValue],
          "title" : channels[index][this.config.apiCreateChannelMap.title.serverValue],
          "checked" : false,
          "identifier" : channels[index][this.config.apiCreateChannelMap.identifier.serverValue],
          "price" : channels[index][this.config.apiCreateChannelMap.inCome.serverValue]});

      }
      this.getChannelReport(channels[0].id);
    }).catch((error) => {
      console.log(error);
    });

  }

  getChannelReport(id) {
    this.reportService.getChannelReport(id).then((data) => {
      this.pieChartData = [ Number(data[this.config.apiChannelReport.totalCount.serverValue]),
                            Number(data[this.config.apiChannelReport.activeCount.serverValue]),
                            Number(data[this.config.apiChannelReport.viewCount.serverValue])];
    }).catch((error) => {
      console.log(error);
    })

  }
}
