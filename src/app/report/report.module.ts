import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report.component';
import {ChartsModule} from "ng2-charts";

@NgModule({
  imports: [
    CommonModule,
    ChartsModule
  ],
  declarations: [ReportComponent]
})
export class ReportModule { }
