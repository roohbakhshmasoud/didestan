import { Injectable } from '@angular/core';
import {AppConfig} from "../app.config";
import {Http} from "@angular/http";
import {HttpClient} from "../HttpClient";

@Injectable()
export class ReportService {
  private channelReportUrl = this.config.baseApiRoot + 'report/channel/';

  constructor(private config: AppConfig,
              private http: HttpClient ) { }


  getChannelReport(id) {
    return this.http.get(this.channelReportUrl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
