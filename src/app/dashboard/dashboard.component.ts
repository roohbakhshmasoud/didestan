import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {LocalStorageService} from "angular-2-local-storage";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private _opened: boolean = true;
  private navbarSelect: string;

  private _toggleSidebar() {
    this._opened = !this._opened;
  }
  constructor(private router: Router,
              private localStorage: LocalStorageService) {
    this.navbarSelect = 'home';
  }

  ngOnInit() {
  }
  changeRoute(route: string, navbar: string) {
    this.navbarSelect = navbar;
    this.router.navigateByUrl(route);
  }
  logout() {
    this.localStorage.clearAll();
    this.router.navigateByUrl('');
  }
}
