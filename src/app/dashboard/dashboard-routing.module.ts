import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard.component";
import {CampaignComponent} from "../campaign/campaign.component";
import {ManageCampaignComponent} from "../manage-campaign/manage-campaign.component";
import {ChannelComponent} from "../channel/channel.component";
import {ChannelViewComponent} from "../channel/channel-view/channel-view.component";
import {AdvertiseComponent} from "../advertise/advertise.component";
import {RouteGaurd} from "../RouteGaurd";
import {ReportComponent} from "../report/report.component";

const routes: Routes = [{path: 'dashboard' , component: DashboardComponent, canActivate: [RouteGaurd],
                         children: [{path: 'createCampaign' , component: CampaignComponent},
                                    {path: 'manageCampaign', component: ManageCampaignComponent},
                                    {path: 'manageCampaign/:id', component: CampaignComponent },
                                    {path: 'channels', component: ChannelComponent },
                                    {path: 'report', component: ReportComponent },
                                    {path: 'channel/view/:id', component: ChannelViewComponent },
                                    {path: 'advertise', component: AdvertiseComponent }]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {  }
