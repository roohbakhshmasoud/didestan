import { Injectable} from '@angular/core';

@Injectable()
export class AppConfig {
    public baseApiRoot = 'http://behrooyanclinic.com:8080/';
    public baseOuthRoot = 'http://behrooyanclinic.com:8030/';
    public uplaodApi = this.baseApiRoot + 'campaign/upload/';
    public vastApi = 'campaign/vast/';
    // campaign_id?api_key='
    public Oauth2 = { login_url : this.baseOuthRoot + 'api/auth/token',
                      grant_type : 'password',
                      client_id : 'Front_#FKKJhxMkn1sSK5Uq2gBwWZFzWQOdqRRUtKiu9Q30pAM='};

    public apiUserLoginMap = {   grantType: { frontValue : '' , serverValue : 'grant_type'},
                                    clientId:  { frontValue : '' , serverValue : 'client_id'},
                                    username: { frontValue : '' , serverValue : 'username'},
                                    password: { frontValue : '' , serverValue : 'password'},
                                    token: { frontValue: '' , serverValue : 'access_token'}};

    public apiCreateChannelMap = {channelId: { frontValue : 'channelId' , serverValue : 'upstream_identifier'},
                                  title:  { frontValue : 'title' , serverValue : 'name'},
                                  price: { frontValue : 'price' , serverValue : 'price_per_view'},
                                  inCome: { frontValue : 'inCome' , serverValue : 'owner_share_per_view'},
                                  file: {frontValue : 'file' , serverValue : 'img'},
                                  id: {frontValue : 'id' , serverValue : 'id'},
                                  identifier: {frontValue : 'id' , serverValue : 'identifier'},
                                  img_uri: {frontValue : 'img_uri' , serverValue : 'img_uri'},
                                  total: {frontValue : 'total' , serverValue : 'total'},
                                  campaigns: {frontValue : 'campaigns' , serverValue : 'campaigns'},
                                  viewCount: {frontValue : 'viewCount' , serverValue : 'view_count' }};

    public apiAdvertiserList = {  id: { frontValue : 'id' , serverValue : 'id'},
                                  title:  { frontValue : 'title' , serverValue : 'name'},
                                  activeCampaign:  { frontValue : 'activeCampaign' , serverValue : 'name'},
                                  campaignList:  { frontValue : 'campaignList' , serverValue : 'campaignList'},
                                  slug: { frontValue : 'price' , serverValue : 'price_per_view'}};

    public apiCampaignCreate = { id: {frontValue : 'id' , serverValue : 'identifier'},
                                 name: {frontValue : 'campaignName' , serverValue : 'name'},
                                 startDate: {frontValue : 'startDate' , serverValue : 'start_date'},
                                 endDate: {frontValue : 'endDate' , serverValue : 'end_date'},
                                 advertisePerson: {frontValue : 'advertisePerson' , serverValue : 'user_id'},
                                 budget: {frontValue : 'budget' , serverValue : 'budget'},
                                 redirectUrl: {frontValue : 'redirectUrl' , serverValue : 'redirect_uri'},
                                 selectedChannels: {frontValue : 'selectedChannels' , serverValue : 'channel_ids'}};

    public apiCampaignList = {   id: {frontValue : 'id' , serverValue : 'identifier'},
                                 name: {frontValue : 'campaignName' , serverValue : 'name'},
                                 channels: {frontValue : 'channels' , serverValue : 'channels'},
                                 activeUser: {frontValue : 'activeUser' , serverValue : 'active_user'},
                                 activeAdmin: {frontValue : 'activeAdmin' , serverValue : 'active_admin'},
                                 budget: {frontValue : 'budget' , serverValue : 'budget'},
                                 remainedBudget: {frontValue : 'remainedBudget' , serverValue : 'remained_budget'},
                                 viewCount: {frontValue : 'viewCount' , serverValue : 'view_count'},
                                 advertiser: {frontValue : 'advertiser' , serverValue : 'user'},
                                 startDate: {frontValue : 'startDate' , serverValue : 'start_date'},
                                 endDate: {frontValue : 'endDate' , serverValue : 'end_date'}};

    public apiUserRegister = {   name: { frontValue : 'name' , serverValue : 'name'},
                                 email:  { frontValue : 'email' , serverValue : 'email'},
                                 password: { frontValue : 'password' , serverValue : 'password'},
                                 verify: { frontValue : 'verify' , serverValue : 'verified'}};

    public apiUserVerify = {     email:  { frontValue : 'email' , serverValue : 'email'},
                                 code: { frontValue : 'code' , serverValue : 'code'}};

    public apiChannelReport = {  activeCount:  { frontValue : 'activeCount' , serverValue : 'campaign_active_count'},
                                 totalCount: { frontValue : 'totalCount' , serverValue : 'campaign_count'},
                                 viewCount: { frontValue : 'viewCount' , serverValue : 'view_count'}};
}
