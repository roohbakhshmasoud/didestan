/**
 * Created by Masoud on 11/17/2017.
 */
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router'
import {LocalStorageService} from "angular-2-local-storage";

@Injectable()
export class RouteGaurd implements CanActivate{

    private user ;
    constructor(private route: Router,
                private localStorage: LocalStorageService) {
    }

    canActivate(route: ActivatedRouteSnapshot ): Promise<boolean> {
        this.user = this.localStorage.get('token');
        console.log('Gaurd is working :).... ');
        return new Promise((resolve) => {
            if (this.user !== null) {
                resolve(true);
            }
            else{
                this.route.navigate(['']);
                resolve(false);
            }
        });
    }

}